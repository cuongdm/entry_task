from django.db import models

class TestTab(models.Model):
	key = models.CharField(max_length=32)
	value = models.CharField(max_length=1024)

	class Config:
		db_for_write = 'test_db.write'
		db_for_read = 'test_db.read'
	
	class Meta:
		db_table = 'test_tab'

class EventTab(models.Model):
    id = models.BigIntegerField()
    name = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    date = models.DateTimeField()
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    event_photo = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    class Meta:
        managed = False
        db_table = 'event_tab'

class UserLikeEventTab(models.Model):
    id = models.BigIntegerField(primary_key=True)
    user_id = models.IntegerField()
    event_id = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'user_like_event_tab'

class UserPartEventTab(models.Model):
    id = models.BigIntegerField()
    user_id = models.IntegerField()
    event_id = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'user_part_event_tab'

class UserTab(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    class Meta:
        managed = False
        db_table = 'user_tab'
 
class UserComtEventTab(models.Model):
    id = models.BigIntegerField(primary_key=True)
    user_id = models.IntegerField()
    event_id = models.IntegerField()
    comment = models.CharField(max_length=300)
    class Meta:
        managed = False
        db_table = 'user_comt_event_tab'
