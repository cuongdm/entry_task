from demolib.models import *
from django.forms.models import model_to_dict
from django.db.models import Q


def get_all_user():
	all_user = UserTab.objects.all()
	return all_user

def check_user(user_name, password):
	return UserTab.objects.filter(name = user_name, password = password)

def get_user_detail(user_id):
	user = None
	try:     
		user = UserTab.objects.get(id=user_id)
	except UserTab.DoesNotExist:
		pass

	return user

def get_all_event():
	all_event = EventTab.objects.all()
	return all_event

def get_event_detail(event_id):
	event = None
	try:     
		event = EventTab.objects.get(id=event_id)
	except EventTab.DoesNotExist:
		pass

	return event

def save_event(name,category,date,title,description,event_photo,location):
	event = EventTab(name = name, category = category,date = date, title = title,description = description, event_photo = event_photo,location = location)
	event.save()

def user_like_event(user_id, event_id):
	# add one like of user with one event
	user_event = UserLikeEventTab.objects.filter(user_id = user_id).filter(event_id = event_id)
	if not user_event:
		user_event = UserLikeEventTab(user_id = user_id, event_id = event_id)
		user_event.save()

def user_part_event(user_id, event_id):
	# add user participant one event row
	user_event = UserPartEventTab.objects.filter(user_id = user_id).filter(event_id = event_id)
	if not user_event:
		user_event = UserPartEventTab(user_id = user_id, event_id = event_id)
		user_event.save()

def user_vote_event(user_id, event_id, comment):
	user_event = UserComtEventTab(user_id = user_id, event_id = event_id, comment = comment)
	user_event.save()		

def search_event(start_date, end_date, cat):
	# search event by start_date, end_date and category
	event_search = Q()
	if start_date:
		event_search = event_search & Q(date__gte = start_date)

	if end_date:
		event_search = event_search & Q(date__lte = end_date)

	if cat:
		event_search = event_search & Q(category = cat)

	return EventTab.objects.filter(event_search)
		
#@db.model_get_optional_result
def get_value_by_key(key):
	return TestTab.objects.filter(key=key)