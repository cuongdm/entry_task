import os
import socket

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "demo.settings")

if socket.socket.__module__ == "gevent.socket":
	import pymysql
	pymysql.install_as_MySQLdb()
	import urls

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
app = application
