from django.conf.urls import patterns, include, url
from demo.views import api

urlpatterns = patterns('',
	url(r'^api/user$', api.user_index, name='user_index'),
	url(r'^api/user/(?P<user_id>\d+)$', api.user_detail, name='user_detail'),
	url(r'^api/event$', api.event_index, name='event_index'),
	url(r'^api/event/(?P<event_id>\d+)$', api.event_detail, name='event_detail'),
	url(r'^api/event/search$', api.event_search, name='event_search'),
	url(r'^api/user/action', api.user_action, name='user_action'),
	url(r'^/?', include('demo_admin.urls', namespace="admin")),
	url(r'^admin/?', include('demo_admin.urls', namespace="admin")),
)