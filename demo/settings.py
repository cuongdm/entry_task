import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DEBUG = True
TEMPLATE_DEBUG = True

TEST = False
TEMPLATE_DEBUG = DEBUG
LOGGER_CONFIG = {
    'log_dir': os.path.join(BASE_DIR, 'log'),
    'sentry_dsn': '{sentry_dsn}',
}
ADMINS = ()
MANAGERS = ADMINS
ALLOWED_HOSTS = ['demo.garena.com','demo.com']
SITE_ID = 1
SECRET_KEY = 'j^0rd4$efh!d-+m8(*%kz%yfm85nk!=8j3sy&d1vu4l^tykhiw'
TIME_ZONE = 'Asia/Singapore'
LANGUAGE_CODE = 'en'
USE_I18N = False
USE_L10N = True
USE_TZ = True
APPEND_SLASH = False
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'raven.contrib.django.raven_compat',
    'demo',
    'demo_admin',
)
MIDDLEWARE_CLASSES = (
    #'common.django_utils.ProxyFixMiddleware',
    'django.middleware.common.CommonMiddleware',
    'raven.contrib.django.middleware.SentryLogMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
)
#SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
#SESSION_COOKIE_SECURE = True
#SESSION_COOKIE_AGE = 3600
#SESSION_COOKIE_NAME = 'session_key'

STATIC_ROOT = ''
STATIC_URL = '/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, '../static').replace('\\', '/'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]
ROOT_URLCONF = 'demo.urls'
WSGI_APPLICATION = 'demo.wsgi.application'
#DATABASE_ROUTERS = ['common.django_model.DatabaseRouter', ]
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'demo_db',
        'USER': 'root',
        'PASSWORD': 'Abcd$1234',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'CONN_MAX_AGE': 7200,
        'OPTIONS': {'charset': 'utf8mb4'},
    },
    'test_db.write': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'demo_db',
        'USER': 'root',
        'PASSWORD': 'Abcd$1234',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'CONN_MAX_AGE': 7200,
        'OPTIONS': {'charset': 'utf8mb4'},
    },
    'test_db.read': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'demo_db',
        'USER': 'root',
        'PASSWORD': 'Abcd$1234',
        'HOST': '127.0.0.2',
        'PORT': '3306',
        'CONN_MAX_AGE': 7200,
        'OPTIONS': {'charset': 'utf8mb4'},
    },
}
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'TIMEOUT': 3000,
        'OPTIONS': {
            'MAX_ENTRIES': 10000
        }
    }
}
'''CACHES = {'default': {
    'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
    'LOCATION': '127.20.100.218:11211',
    'TIMEOUT': 7 * 24 * 60 * 60,
    'KEY_PREFIX': 'demo',
    'VERSION': 1,
},
}'''
'''CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}'''
'''EXTRA_CACHES = {
    'main': {
        'type': 'memcached',
        'host': '10.0.0.3',
        'port': 11211,
        'default_timeout': 7 * 24 * 60 * 60,
        'default': True,
    },
}'''
GEOIP_PATH = '/usr/share/GeoIP/'
HTTP_PROXY_FIX = True
STATIC_URL = '/'
if DEBUG or TEST:
    #LOGGER_CONFIG['sentry_dsn'] = '{sentry_dsn_test}'
    ALLOWED_HOSTS = ['demo.test.garena.com','demo.com']
    if DEBUG:
        STATIC_URL = '/static/'
    for key in DATABASES:
        db = DATABASES[key]
        db['USER'] = 'root'
        db['HOST'] = '127.0.0.1'
    if DEBUG:
        CACHES['default']['host'] = '127.0.0.1'
        #EXTRA_CACHES['main']['host'] = '127.0.0.1'
