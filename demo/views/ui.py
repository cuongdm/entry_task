#from common.utils import *
#from common import template_utils
from django.views.decorators.cache import cache_control
from demolib.data_schema import *
from django.shortcuts import render
from demolib import db_manager

@cache_control(no_cache=True)
#@log_request(log_response=False)
#@parse_params(MainSchema, 'GET')

def view_main(request, data=None):
	if (data != None):
		data['value'] = db_manager.get_value_by_key(data['key'])
		return render(request, 'index.html', data)
	else:
		return render(request, 'index.html')