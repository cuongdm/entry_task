from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from demolib import db_manager
from demolib.constant import *
from django.core import serializers
import json

# Create your views here.
def user_index(request):
	all_user = db_manager.get_all_user();
	all_user_info = {a.id:a.name for a in all_user}

	return HttpResponse(json.dumps(all_user_info), content_type="application/json")

def user_detail(request, user_id):
	#return db_manager.get_all_user();
	result = {}
	user = db_manager.get_user_detail(user_id)
	if user:
		result = {user.id:{'name':user.name,'password':user.password}}
	return HttpResponse(json.dumps(result), content_type="application/json")

def event_index(request):
	all_event = db_manager.get_all_event();
	all_event_info = {a.id:a.name for a in all_event}

	return HttpResponse(json.dumps(all_event_info), content_type="application/json")

def event_search(request):
	result = {}
	start_date = request.GET.get("start_date","")
	end_date = request.GET.get("end_date", "")
	cat = request.GET.get("cat", "")
	page = int(request.GET.get("page", 0))
	size = int(request.GET.get("size", 0))

	all_event = db_manager.search_event(start_date, end_date, cat)
	if size != 0:
		start_idx = page * size
		end_idx = (page + 1) * size

		if start_idx >= len(all_event):
			all_event = []
		elif end_idx >= len(all_event):
			all_event = all_event[start_idx:]
		else:
			all_event = all_event[start_idx:end_idx]
	all_event_info = {a.id:a.name for a in all_event}

	return HttpResponse(json.dumps(all_event_info), content_type="application/json")

def user_action(request):
	#return db_manager.get_all_user();
	result = {'status':1}
	user_id = request.GET.get("user_id","")
	action = request.GET.get("action", API_ACTION_PART)
	event_id = request.GET.get("event_id","")
	comment = request.GET.get("comment","")

	if user_id == "":
		result = {'status': API_RESULT_UNSUCCESSFUL, 'reason': API_RESULT_NOUSER}
	elif event_id == "":
		result = {'status': API_RESULT_UNSUCCESSFUL, 'reason': API_RESULT_NOEVENT}	
	else:
		user = db_manager.get_user_detail(user_id)
		event = db_manager.get_event_detail(event_id)
		if not user:
			result = {'status': API_RESULT_UNSUCCESSFUL, 'reason': API_RESULT_USERNOTFOUND}	
		elif not event:
			result = {'status': API_RESULT_UNSUCCESSFUL, 'reason': API_RESULT_EVENTNOTFOUND}
		elif action not in (API_ACTION_LIKE, API_ACTION_PART, API_ACTION_VOTE):
			result = {'status': API_RESULT_UNSUCCESSFUL, 'reason': API_RESULT_ACTIONNOTFOUND}
		else:
			if action == API_ACTION_LIKE:
				db_manager.user_like_event(user_id, event_id)
			elif action == API_ACTION_PART:
				db_manager.user_part_event(user_id, event_id)
			else:
				db_manager.user_vote_event(user_id, event_id, comment)


	return HttpResponse(json.dumps(result), content_type="application/json")

def event_detail(request,event_id):
	result = {}
	event = db_manager.get_event_detail(event_id)
	if event:
		result = {event.id:{'id':event.id,'name':event.name}}

	return HttpResponse(json.dumps(result), content_type="application/json")
