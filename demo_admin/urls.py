from django.conf.urls import patterns, url

from demo_admin import views

urlpatterns = patterns('',
    url(r'^/?$', views.index, name='index'),
    url(r'^login/?$', views.login, name='login'),
    url(r'^logout/?$', views.logout, name='logout'),
    url(r'^create/?$', views.create, name='create'),
    url(r'^event/', views.save, name='save'),
)