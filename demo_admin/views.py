from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import render, get_object_or_404
from django.core.urlresolvers import reverse
from django.core.files.storage import FileSystemStorage
from demolib import db_manager
from demolib.constant import *

# Create your views here.
def index(request):
	user = request.session.get('user', False)
	if user:
		return render(request, 'admin/index.html', {'user':user, 'events': db_manager.get_all_event()})
	else:
		return render(request, 'admin/login.html', {'user':user})

def login(request):
	username = request.POST.get("username", "")
	password = request.POST.get("password", "")

	if username == "" and password == "":
		user = request.session.get('user', False)
		if user:
			return render(request, 'admin/index.html',{'user':user, 'events': db_manager.get_all_event()})

	# check username password
	user = db_manager.check_user(username, password)
	if len(user)>0:
		request.session['user'] = user[0].id
		return render(request, 'admin/index.html',{'user':user[0].id, 'events': db_manager.get_all_event()})
	else:
		return render(request, 'admin/login.html')

def logout(request):
	del request.session['user']
	return render(request, 'admin/login.html')

    
def save(request):
	name = request.POST.get("name", "")
	category = request.POST.get("category", "")
	date = request.POST.get("date", "")
	title = request.POST.get("title", "")
	description = request.POST.get("description", "")
	event_photo = ""
	if request.FILES['event_photo']:
		myfile = request.FILES['event_photo']
		fs = FileSystemStorage()
		filename = fs.save(UPLOAD_FOLDER+myfile.name, myfile)
		event_photo = fs.url(filename)
	location = request.POST.get("location", "")
	
	user = request.session.get('user', False)
	db_manager.save_event(name,category,date,title,description,event_photo,location)
	return render(request, 'admin/index.html', {'user':user, 'events': db_manager.get_all_event()})
    
def create(request):
	return render(request, 'admin/create.html')

def detail(request):
    template = loader.get_template('admin/index.html')
    return HttpResponse(template.render(context))

def results(request):
    template = loader.get_template('admin/index.html')
    return HttpResponse(template.render(context))

def vote(request):
    template = loader.get_template('admin/index.html')
    return HttpResponse(template.render(context))